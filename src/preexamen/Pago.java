package preexamen;

/**
 *
 * @author jesus
 */
public class Pago {
     //Atributos de la clase
    private int numCon;
    private String nomEmp;
    private String dom;
    private int tipoCon;
    private int nivelEst;
    private float pdBase;
    private int diasTrabajados;
    private String fechaPago;
    private String fechaCon;
    
    public Pago(){
    this.numCon=0;
    this.nomEmp="";
    this.dom="";
    this.tipoCon=0;
    this.nivelEst=0;
    this.pdBase=0.0f;
    this.diasTrabajados=0;
    this.fechaPago="";
    this.fechaCon="";
    
    }
//Constructor por argumentos
    public Pago(int numCon, String nomEmp, String dom, int tipoCon, int nivelEst, float pdBase, int diasTrabajados, String fechaPago, String fechaCon){
    this.numCon=numCon;
    this.nomEmp=nomEmp;
    this.dom=dom;
    this.tipoCon=tipoCon;
    this.nivelEst=nivelEst;
    this.pdBase=pdBase;
    this.diasTrabajados=diasTrabajados;
    this.fechaPago=fechaPago;
    this.fechaPago=fechaCon;
    this.fechaCon=fechaCon;
    }
    
    //Copia
    public Pago(Pago otro){
     this.numCon=otro.numCon;
    this.nomEmp=otro.nomEmp;
    this.dom=otro.dom;
    this.tipoCon=otro.tipoCon;
    this.nivelEst=otro.nivelEst;
    this.pdBase=otro.pdBase;
    this.diasTrabajados=otro.diasTrabajados;
    this.fechaPago=otro.fechaPago;
    this.fechaCon=otro.fechaCon;
    }
    
    //Metodos Set y Get

    public int getNumCon() {
        return numCon;
    }

    public void setNumCon(int numCon) {
        this.numCon = numCon;
    }

    public String getNomEmp() {
        return nomEmp;
    }

    public void setNomEmp(String nomEmp) {
        this.nomEmp = nomEmp;
    }

    public String getDom() {
        return dom;
    }

    public void setDom(String dom) {
        this.dom = dom;
    }

    public int getTipoCon() {
        return tipoCon;
    }

    public void setTipoCon(int tipoCon) {
        this.tipoCon = tipoCon;
    }

    public int getNivelEst() {
        return nivelEst;
    }

    public void setNivelEst(int nivelEst) {
        this.nivelEst = nivelEst;
    }

    public float getPdBase() {
        return pdBase;
    }

    public void setPdBase(float pdBase) {
        this.pdBase = pdBase;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getFechaCon() {
        return fechaCon;
    }

    public void setFechaCon(String fechaCon) {
        this.fechaCon = fechaCon;
    }
    
    //Metodos de comportamiento
    public float calcularSubtotal(){
    float subtotal=0.0f;
    if (this.nivelEst == 1) {
        subtotal=(this.diasTrabajados*(this.pdBase+this.pdBase*0.20f));
    }
    if (this.nivelEst == 2) {
        subtotal=(this.diasTrabajados*(this.pdBase+this.pdBase*0.50f));
    }
    if (this.nivelEst == 3) {
        subtotal=(this.diasTrabajados*(this.pdBase+this.pdBase)); 
    }
    return subtotal;
    }
    
    public float calcularImpuesto(){
    float impuesto=0.0f;
    impuesto=this.calcularSubtotal()*0.16f;
    return impuesto;
    }
    
    public float calcularTotal(){
    float total=0.0f;
    total=this.calcularSubtotal()-this.calcularImpuesto();
    return total;
    }
    
}


