package preexamen;

/**
 *
 * @author jesus
 */
public class PreExamen {

    public static void main(String[] args) {
        // TODO code application logic here
         // TODO code application logic here
        Pago pago = new Pago();
        pago.setNumCon(102);
        pago.setNomEmp("jose lopez");
        pago.setDom("av del sol 1200");
        pago.setTipoCon(1);
        pago.setNivelEst(1);
        pago.setPdBase(700.0f);
        pago.setDiasTrabajados(15);
        pago.setFechaPago("21 marzo 2019");
        
        System.out.println("Calculo de pago subtotal:"+ pago.calcularSubtotal());
        System.out.println("calculo de pago con impuesto:"+ pago.calcularImpuesto());
        System.out.println("calculo de pago total:"+ pago.calcularTotal());
        
        
        System.out.println("-----------------------------------------------------------------------");
        
        pago.setNumCon(103);
        pago.setNomEmp("maria acosta");
        pago.setDom("av del sol 1200");
        pago.setTipoCon(2);
        pago.setNivelEst(2);
        pago.setPdBase(700.0f);
        pago.setDiasTrabajados(15);
        pago.setFechaCon("20 marzo 2019");
        
        System.out.println("Calculo de pago subtotal:"+ pago.calcularSubtotal());
        System.out.println("calculo de pago con impuesto:"+ pago.calcularImpuesto());
        System.out.println("calculo de pago total:"+ pago.calcularTotal());
    }
}

    
